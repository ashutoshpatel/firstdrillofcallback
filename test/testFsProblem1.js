// import the functions
const obj = require('../fs-problem1');

obj.createJsonFiles((error, createdFilesName) => {
    if (error) {
        console.log('Error of creating random JSON files:', error);
        return;
    }

    console.log('Random JSON files are created:', createdFilesName);

    obj.deleteJsonFiles(createdFilesName, (error) => {
        if (error) {
            console.log('Error of deleting files:', error);
            return;
        }

        console.log('Files deleted successfully.');
    });
});