// import the functions
const obj = require('../fs-problem2');

obj.readTheFile('../lipsum_1.txt', (error1, data1)=>{
    if(error1){
        console.log('Error to reading lipsum_1 file: ', error1);
        console.log('\n');
        return;
    }

    console.log("Read the lipsum file content: ", data1);
    console.log('\n');


    obj.createNewFileWithUpperCase('../uppercase.txt', data1, (error2, success)=>{
        if(error2){
            console.log('Error to creating new uppercase file: ', error2);
            return;
        }

        console.log("File created successfully with upper-case", success);
        console.log('\n');

        obj.readTheFile('../uppercase.txt', (error3, data2)=>{
            if(error3){
                console.log('Error to reading uppercase file: ', error3);
                console.log('\n');
                return;
            }
        
            console.log("Read the uppercase file content: ", data2);
            console.log('\n');
    
            obj.covertFileInLowerCaseWithSplit('../lowercase.txt', data2, (error4, data3) => {
                if(error4){
                    console.log('Error to create new file with lower case + split: ', error3);
                    console.log('\n');
                    return;
                }
        
                console.log("File created successfully with lower-case and split: ", data3);
                console.log('\n');

                obj.readTheFile('../lowercase.txt', (error5, data4)=>{
                    if(error5){
                        console.log('Error to reading lower case file: ', error5);
                        console.log('\n');
                        return;
                    }
                
                    console.log("Read the lowercase file content: ", data4);
                    console.log('\n');
            
                    obj.sortTheContentOfFile('../sortcontent.txt', data4, (error5, data5) => {
                        if(error5){
                            console.log('Error to create new file with sort the content: ', error3);
                            console.log('\n');
                            return;
                        }
                
                        console.log("File created successfully with sorting the content: ", data5);
                        console.log('\n');

                        obj.readTheFile('../sortcontent.txt', (error6, data5)=>{
                            if(error6){
                                console.log('Error to reading sort content file: ', error6);
                                console.log('\n');
                                return;
                            }
                        
                            console.log("Read the sort content file content: ", data5);
                            console.log('\n');

                            obj.readTheFile('../filenames.txt', (error7, filesName)=>{
                                if(error7){
                                    console.log('Error to reading file name file: ', error7);
                                    console.log('\n');
                                    return;
                                }
                                
                                filesName = filesName.split(',');
                                
                                obj.deleteTheFiles(filesName, (error8) => {
                                    if (error8) {
                                        console.log('Error of deleting files:', error8);
                                        return;
                                    }
                            
                                    console.log('Files deleted successfully.');
                                });
                                
                            });
                    
                        });

                    });
            
                });
            });
    
        });

    });

});