const { error } = require('console');
const fs = require('fs');

function readTheFile(path, cb){

    fs.readFile(path, 'utf-8', (error, data) =>{
        if(error){
            return cb(error);
        }

        return cb(null, data);
    });
}

function createNewFileWithUpperCase(path, data, cb){

    fs.writeFile(path, data.toUpperCase(), (error) =>{
        if(error){
            return cb(error);
        }

        fs.appendFile('../filenames.txt', path+',', (error) => {
            if(error){
                console.log(error);
            }
        });

        cb(null, path);
    })
}

function covertFileInLowerCaseWithSplit(path, content, cb){

    let data = content.toLowerCase();
    data = data.split('.');
    
    fs.writeFile(path, data.join("\n"), (error) => {
        if(error){
            return cb(error);
        }
        
        fs.appendFile('../filenames.txt', path+',', (error) => {
            if(error){
                console.log(error);
            }
        });

        cb(null, path);
    });
}

function sortTheContentOfFile(path, content, cb){

    const data = content.split('.');
    data.sort();
    
    fs.writeFile(path, data.join('\n'), (error) => {
        if(error){
            return cb(error);
        }

        fs.appendFile('../filenames.txt', path+',', (error) => {
            if(error){
                console.log(error);
            }
        });

        cb(null, path);
    });
}

function deleteTheFiles(files, cb){
    let numberOfFilesDeleted = 0;

    files.forEach( (curentFile) => {
        fs.unlink(curentFile, (error) => {

            if(curentFile === ""){
                return;
            }

            // if we got error to remove file
            if(error){
                // then return error
                return cb(error);
            }

            // update the number of delete file
            numberOfFilesDeleted++;

            if(numberOfFilesDeleted === files.length-1){
                cb(null);
            }
        });
    });

    fs.unlink('../filenames.txt', (error)=>{
        if(error){
            console.log("Error to removing file name file");
            return;
        }
        
        console.log('File name file is remove successfully');
    });
}

// export the functions
module.exports = {
    readTheFile,
    createNewFileWithUpperCase,
    covertFileInLowerCaseWithSplit,
    sortTheContentOfFile,
    deleteTheFiles,
}