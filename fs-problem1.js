const { error } = require('console');
const fs = require('fs');

const directoryName = '../random_json_file';
const numberOfFiles = 3;

function createJsonFiles(cb){
    fs.mkdir(directoryName, (error) => {

        // if we got error to create directory
        if(error){
            // then return error
            return cb(error);
        }

        // create array to store all file names which is created
        let createdFilesName = [];

        for(let index=1; index<=numberOfFiles; index++){
            const createFileName = `file_${index}.json`;
            const fileContent = { data : Math.random() };
            const path = directoryName+ '/' + createFileName;

            fs.writeFile(path, JSON.stringify(fileContent), (error) => {
                
                // if we got error to create file
                if(error){
                    // then return error
                    return cb(error);
                }

                // add create file name in array
                createdFilesName.push(path);

                if(createdFilesName.length === numberOfFiles){
                    cb(null, createdFilesName);
                }

            });
        }
    });
}

function deleteJsonFiles(files, cb){
    let numberOfFilesDeleted = 0;

    files.forEach( (currentFile) => {
        fs.unlink(currentFile, (error) => {
            
            // if we got error to remove file
            if(error){
                // then return error
                return cb(error);
            }

            // update the number of delete file
            numberOfFilesDeleted++;

            if(numberOfFilesDeleted === files.length){
                cb(null);
            }
        });
    });

    // remove directory
    fs.rmdir(directoryName, (error)=>{
        if(error){
            console.log("Error to removing directory");
            return;
        }
        
        console.log('Directory remove successfully');
    });
}

// export the functions
module.exports = {
    createJsonFiles,
    deleteJsonFiles,
}